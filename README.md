# zirkel-schubfachprinzip

Mit viel Inspiration von tollen Videos:
* [Mathologer: The Pigeon Hole Principle: 7 gorgeous proofs](https://www.youtube.com/watch?v=TCZ3YwbcDaw)
* [Spanning Tree: What Is the Pigeonhole Principle?](https://www.youtube.com/watch?v=B2A2pGrDG8I)
* [Up and Atom: The Impossible Power of This Simple Math Proof](https://www.youtube.com/watch?v=tiPdN0wtN0M)